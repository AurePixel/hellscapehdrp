﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Hellscape.Movement
{
    /// <summary>
    /// Classe d'aide aux movements des objects dans l'espace.
    /// </summary>
    public static class MovementHelper
    {
        public static void Move(Rigidbody rb, Vector3 direction, float speed)
        {
            rb.MovePosition(rb.transform.position + direction.normalized * speed * Time.fixedDeltaTime);
        }

        public static void Move(Rigidbody rb, Transform target, float speed, float range)
        {
            var direction = (target.position - rb.transform.position);
            if (direction.magnitude > range)
                rb.MovePosition(rb.transform.position + direction.normalized * speed * Time.fixedDeltaTime);
        }

        public static void Look(Transform transform, Vector3 pointToLook)
        {
            pointToLook.y = transform.position.y;
            transform.LookAt(pointToLook);
        }

        public static void LookSmooth(Transform transform, Vector3 direction, float rotationSpeed)
        {
            var targetRotation = Quaternion.LookRotation(direction);

            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed * Time.fixedDeltaTime);
        }
    }
}

