﻿using System;
using UnityEngine;

namespace Hellscape.MenuScripts
{
    public abstract class UiEntity : MonoBehaviour
    {

        protected virtual void Init()
        {
        }
        public abstract void Active();
        public abstract void Inactive();

        public abstract bool IsActive();
    }
}