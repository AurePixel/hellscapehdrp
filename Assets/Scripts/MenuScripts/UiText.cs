﻿using UnityEngine;
using UnityEngine.UI;

namespace Hellscape.MenuScripts
{
    public class UiText : UiEntity
    {
        [SerializeField] protected Text _textArea;

        protected void Start()
        {
            Init();
        }

        protected override void Init()
        {
            base.Init();
            if (!_textArea) _textArea = GetComponent<Text>();
        }
        
        public override void Active()
        {
            throw new System.NotImplementedException();
        }

        public override void Inactive()
        {
            throw new System.NotImplementedException();
        }

        public override bool IsActive()
        {
            throw new System.NotImplementedException();
        }
    }
}