﻿using UnityEngine;

namespace Hellscape.MenuScripts.ButtonScripts
{
	public class QuitButton : UiButton
	{
		public override void OnClick()
		{
			base.OnClick();
			Application.Quit();
		}
	}
}
