﻿using UnityEngine.SceneManagement;

namespace Hellscape.MenuScripts.ButtonScripts
{
    public class PlayButton : UiButton
    {
        public override void OnClick()
        {
            base.OnClick();
            SceneManager.LoadScene("InGame");
        }
    }
}