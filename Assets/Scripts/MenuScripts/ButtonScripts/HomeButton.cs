﻿using UnityEngine.SceneManagement;
namespace Hellscape.MenuScripts.ButtonScripts
{
    public class HomeButton : UiButton
    {
        public override void OnClick()
        {
            base.OnClick();
            
            SceneManager.LoadScene("Start");
        }
    }
}