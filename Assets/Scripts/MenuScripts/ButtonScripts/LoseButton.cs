﻿using UnityEngine.SceneManagement;

namespace Hellscape.MenuScripts.ButtonScripts
{
    public class LoseButton : UiButton
    {
        public override void OnClick()
        {
            base.OnClick();
            SceneManager.LoadScene(GameState.EndGame.ToString());
        }
    }
}