﻿using UnityEngine.SceneManagement;

namespace Hellscape.MenuScripts.ButtonScripts
{
    public class TalentTreeButton : UiButton
    {
        public override void OnClick()
        {
            base.OnClick();
            SceneManager.LoadScene("TalentTree");
        }
    }
}
