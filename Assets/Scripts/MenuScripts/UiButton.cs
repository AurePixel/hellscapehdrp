﻿using UnityEngine;
using UnityEngine.UI;

namespace Hellscape.MenuScripts
{
    [RequireComponent(typeof(Button))]
    [RequireComponent(typeof(AudioSource))]
    public class UiButton : UiEntity, IButton
    {

        private Button _button;
        [SerializeField] private AudioSource _audioSource;

        protected void Start()
        {
            Init();
        }

        protected override void Init()
        {
            base.Init();
            _button = GetComponent<Button>();
            _audioSource = GetComponent<AudioSource>();
        }

        public override void Active()
        {
            if (_button) _button.interactable = true;
        }

        public override void Inactive()
        {
            if (_button) _button.interactable = false;
        }

        public override bool IsActive()
        {
            return _button && _button.interactable;
        }

        public virtual void OnClick()
        {
            // PlaySound(_clickSound);
            _audioSource.Play();
        }

        public void PlaySound(AudioClip sound)
        {
            if(_audioSource&&sound) _audioSource.PlayOneShot(sound);
        }
    }
}