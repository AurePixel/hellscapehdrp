﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Hellscape.MenuScripts
{
    public class Loading : MonoBehaviour
    {
        [SerializeField] private AudioSource _audioSource;

        private bool _keyPressed = false;
        
        private void FixedUpdate() 
        {
            if (Input.anyKey && !_keyPressed)
            {
                _keyPressed = true;
                
                _audioSource.Play();

                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            }
        }        
    }
}