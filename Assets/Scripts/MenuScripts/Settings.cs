﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

namespace Hellscape.MenuScripts
{
    [Serializable]
    public class Settings : MonoBehaviour
    {
        [Header("Audio")]
        [SerializeField] private AudioMixer audioMixer;
    
        [Header("Screen")]
        [SerializeField] private Dropdown resolutionDropdown;
        [SerializeField] private Dropdown screenDropdown;
    
        private String[] _screens;
        private Resolution[] _resolutions;

        private void Start()
        {
            GetScreen();
            GetResolution();
        }

        // Audio
    
        public void SetMaster(float volume)
        {
            audioMixer.SetFloat("MasterVolume", volume);
        }
    
        public void SetAmbient(float volume)
        {
            audioMixer.SetFloat("AmbientVolume", volume);
        }

        public void SetFX(float volume)
        {
            audioMixer.SetFloat("FXVolume", volume);
        }
    
        // Screen
        public void GetScreen()
        {
            _screens = Enum.GetNames(typeof(FullScreenMode));
        
            screenDropdown.ClearOptions();
        
            List<String> options = new List<string>();
            int currentScreen = 0;
        
            for (int i = 0; i < _screens.Length; i++)
            {
                options.Add(_screens[i]);
                if (_screens[i] == Screen.fullScreenMode.ToString())
                {
                    currentScreen = i;
                }
            }
        
            screenDropdown.AddOptions(options);
            screenDropdown.value = currentScreen;
            screenDropdown.RefreshShownValue();
        }

        public void GetResolution()
        {
            _resolutions = Screen.resolutions;

            resolutionDropdown.ClearOptions();

            List<String> options = new List<string>();

            int currentResolutionIndex = 0;

            for (int i = 0; i < _resolutions.Length; i++)
            {
                string option = _resolutions[i].width + " x " + _resolutions[i].height;
                options.Add(option);

                if (_resolutions[i].width == Screen.currentResolution.width && _resolutions[i].height == Screen.currentResolution.height)
                {
                    currentResolutionIndex = i;
                }
            }
        
            resolutionDropdown.AddOptions(options);
            resolutionDropdown.value = currentResolutionIndex;
            resolutionDropdown.RefreshShownValue();
        }
    }
}
