﻿using System;
using Hellscape.Core;
using Hellscape.Entity.Player;
using UnityEngine;

namespace Hellscape.MenuScripts.Texts
{
    public class SoulsText: UiText
    {
        [SerializeField] private PlayerAttribute _playerAttribute;

        protected override void Init()
        {
            base.Init();
            _textArea.text = _playerAttribute.GetSouls().ToString();
        }

        public void Update()
        {
            _textArea.text = _playerAttribute.GetSouls().ToString();
        }
    }
}