﻿using UnityEngine;

namespace Hellscape.MenuScripts.TalentTree
{
    [CreateAssetMenu(fileName = "TalentData", menuName = "Talent/Talent Data")]
    public class TalentData : ScriptableObject
    {
        public string Name;
        public string Description;
        public Sprite Sprite;
        public Material LavaSymbolMaterial;
    }
}
