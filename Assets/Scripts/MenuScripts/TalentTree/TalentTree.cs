﻿using System.Collections.Generic;
using Hellscape.Core;
using Hellscape.MenuScripts.Texts;
using TMPro;
using UnityEngine;

namespace Hellscape.MenuScripts.TalentTree
{
    public class TalentTree : MonoBehaviour
    {
        [SerializeField] private TalentPopupSystem _talentPopupSystem;
        [SerializeField] private DialogPopupSystem _dialogPopupSystem;
        [SerializeField] private InfoPopupSystem _infoPopupSystem;
        [SerializeField] private SoulsText _soulCounterText;
        [SerializeField] private List<Talent> _talents;
        [SerializeField] private List<Talent> _talentsUnlocked;
        [SerializeField] private List<Talent> _talentsBuyed;

        private Talent _currentTalent;

        private void Start() 
        {
            ResetTalents();
        }

        private void ResetTalents()
        {
            _soulCounterText.Update();

            foreach (Talent talent in _talents)
                talent.Lock();

            foreach (Talent talent in _talentsUnlocked)
                talent.Unlock();
        }

        public void TryUseTalent()
        {
            if (_currentTalent == null || !_currentTalent.IsClickable() || 
                _currentTalent.GetCurrentState() == TalentState.Buyed)
                return;

            if (SoulPoints <= 0 ) 
            {
                _infoPopupSystem.DisplayingPopup(true);
                return;
            }

            SoulPoints--;
            _currentTalent.BuyTalent();
            _talentsBuyed.Add(_currentTalent);
        }

        public int SoulPoints
        {
            get { return PlayerPrefs.GetInt(CONSTANTS.SOULS_COUNTER); }
            set 
            { 
                PlayerPrefs.SetInt(CONSTANTS.SOULS_COUNTER, value); 
                _soulCounterText.Update();
            }
        }

        public void OnTalentClicked(Talent talent)
        {
            _talentPopupSystem.SetTalentInfos(talent.GetCurrentState(), talent.GetTalentData());
            _dialogPopupSystem.SetTalentInfos(talent.GetCurrentState(), talent.GetTalentData());

            _talentPopupSystem.Popup();

            _currentTalent = talent;
        }
    }
}