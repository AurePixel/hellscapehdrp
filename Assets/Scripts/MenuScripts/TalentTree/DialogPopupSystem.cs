using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Hellscape.MenuScripts.TalentTree
{
    public class DialogPopupSystem : PopupSystem
    {
        [SerializeField] private Button _buttonNo;
        [SerializeField] private Button _buttonYes;
        [SerializeField] private TMP_Text _dialogText;

        private const string _BUY_TEXT = "Voulez-vous acheter ce talent ?";
        private const string _ENABLE_TEXT = "Voulez-vous activer ce talent ?";

        private const string _DIALOG_DISPLAYED = "DialogDisplayed";

        private void ChangeDialogText(TalentState talentState)
        {
            if (talentState == TalentState.Buyed)
                _dialogText.text = _ENABLE_TEXT;
            else
                _dialogText.text = _BUY_TEXT;
        }

        public override void DisplayingPopup(bool displayed)
        {
            animator.SetBool(_DIALOG_DISPLAYED, displayed);

            audioSource.Play();
        }

        public override void SetTalentInfos(TalentState state, TalentData talentData)
        {
            talentName.text = talentData.Name;
            talentSymbol.sprite = talentData.Sprite;

            ChangeDialogText(state);
        }
    }
}
