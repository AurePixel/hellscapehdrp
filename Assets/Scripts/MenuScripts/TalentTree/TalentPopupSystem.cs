using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Hellscape.MenuScripts.TalentTree
{
    public class TalentPopupSystem : PopupSystem
    {
        [SerializeField] private TMP_Text _actionButtonText;
        [SerializeField] private Button _quitButton;
        [SerializeField] private TMP_Text _talentDescription;

        private const string _DISPLAYED = "Displayed";

        private void ChangeActionButtonText(TalentState talentState)
        {
            switch (talentState)
            {
                case TalentState.Lock:
                    _actionButtonText.text = "Unlock";
                    break;

                case TalentState.Unlock:
                    _actionButtonText.text = "Buy";
                    break;

                case TalentState.Buyed:
                    _actionButtonText.text = "Activate";
                    break;

                case TalentState.Activated:
                    _actionButtonText.text = "Deactivate";
                    break;

                default:
                    _actionButtonText.text = "Unlock";
                    break;
            }
        }

        public override void DisplayingPopup(bool displayed)
        {
            animator.SetBool(_DISPLAYED, displayed);

            audioSource.Play();
        }

        public override void SetTalentInfos(TalentState state, TalentData talentData)
        {
            _talentDescription.text = talentData.Description;
            talentName.text = talentData.Name;
            talentSymbol.sprite = talentData.Sprite;

            ChangeActionButtonText(state);
        }
    }
}
