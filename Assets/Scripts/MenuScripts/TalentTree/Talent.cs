﻿using UnityEngine;
using UnityEngine.UI;

namespace Hellscape.MenuScripts.TalentTree
{
    public enum TalentState 
    {
        Lock,
        Unlock,
        Buyed,
        Activated
    }

    public class Talent : MonoBehaviour
    {
        [SerializeField] private TalentState _currentState;
        [SerializeField] private Image[] _arrowImages;
        [SerializeField] private Talent[] _childrenTalents;
        [SerializeField] private Image _rockImage;
        [SerializeField] private Image _symbolImage;
        [SerializeField] private TalentData _talentData;

        private void Awake() 
        {
            if (_currentState == TalentState.Unlock)
                Unlock();

            if (!PlayerPrefs.HasKey(_talentData.Name))
                PlayerPrefs.SetInt(_talentData.Name, _currentState == TalentState.Buyed ? 1 : 0);

        }

        public virtual void Lock()
        {
            _rockImage.color = Color.gray;

            for (int i = 0; i < _arrowImages.Length; i++)
                _arrowImages[i].gameObject.SetActive(false);
        }

        public virtual void BuyTalent()
        {
            _currentState = TalentState.Buyed;

            // Add lava material to symbol
            _symbolImage.material = _talentData.LavaSymbolMaterial;

            // Unlock children talents
            if (_childrenTalents != null)
            {
                for (int i = 0; i < _childrenTalents.Length; i++)
                    _childrenTalents[i].Unlock();       
            }

            // Enable lava trail
            for (int i = 0; i < _arrowImages.Length; i++)
                _arrowImages[i].gameObject.SetActive(true);
        }

        public void Unlock()
        {
            _rockImage.color = Color.white;

            _currentState = TalentState.Unlock;
        }

        public bool IsClickable()
        {
            if (_currentState == TalentState.Lock)
                return false;

            if (_currentState == TalentState.Buyed)
                return false;

            return true;
        }

        public TalentData GetTalentData()
        {
            return _talentData;
        }

        public TalentState GetCurrentState()
        {
            return _currentState;
        }
    }
}