﻿namespace Hellscape.MenuScripts.TalentTree
{
    public class InfoPopupSystem : PopupSystem
    {
        private const string _SHOW_INFO = "Show";

        public override void DisplayingPopup(bool showed)
        {
            animator.SetBool(_SHOW_INFO, showed);

            audioSource.Play();
        }
    }
}
