﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Hellscape.MenuScripts.TalentTree
{
    public class PopupSystem : MonoBehaviour
    {
        [SerializeField] protected Animator animator;
        [SerializeField] protected AudioSource audioSource;
        [SerializeField] protected GameObject popupBox;
        [SerializeField] protected TMP_Text talentName;
        [SerializeField] protected Image talentSymbol;

        public virtual void Popup()
        {
            popupBox.SetActive(true);

            DisplayingPopup(true);
        }

        public virtual void DisplayingPopup(bool displayed)
        {

        }

        public virtual void SetTalentInfos(TalentState state, TalentData talentData)
        {

        }
    }
}
