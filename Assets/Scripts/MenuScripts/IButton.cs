﻿using UnityEngine;

namespace Hellscape.MenuScripts
{
    public interface IButton
    {
        void OnClick();
        void PlaySound(AudioClip sound);
    }
}
