﻿using UnityEngine;

namespace Hellscape.MenuScripts
{
    public class UiPanel : UiEntity
    {
        [SerializeField] private UiPanel[] _panels;
        [SerializeField] private AudioSource _audioSource;
        private int _currentPanelIndex = 0;
        private UiPanel _currentPanel;
        protected void Start()
        {
            Init();
        }

        protected override void Init()
        {
            base.Init();
        }

        public void Return()
        {
            _audioSource.Play();
            ChangePanels(_currentPanelIndex -1 );
        }

        public void Next()
        {   _audioSource.Play();
            ChangePanels(_currentPanelIndex + 1 );
            
        }

        public void PlaySound(AudioClip sound)
        {
            if(_audioSource&&sound) _audioSource.PlayOneShot(sound);
        }

        private void ChangePanels(int index)
        {
            if(!_currentPanel) _currentPanel = _panels[0];
            if(_currentPanel) _currentPanel.Inactive();
            if (index < _panels.Length && index > -1)
            {
                _currentPanel = _panels[index];
                _currentPanel.Active();
            }
            _currentPanelIndex = index;
        }

        public override void Active()
        {
            gameObject.SetActive(true);
        }

        public override void Inactive()
        {
            gameObject.SetActive(false);
        }

        public override bool IsActive()
        {
            return gameObject.activeInHierarchy;
        }
    }
}