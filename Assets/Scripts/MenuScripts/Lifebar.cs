﻿using System;
using Hellscape.Entity.SharedData;
using UnityEngine;
using UnityEngine.UI;

namespace Hellscape.MenuScripts
{
    public class Lifebar: UiEntity
    {
        [SerializeField] private EntityStats _stats;
        [SerializeField] private Scrollbar _scrollbar;
        private void Start()
        {
            _scrollbar.size = _stats.GetHealth()/_stats.MaxHealth;
        }

        private void Update()
        {
            _scrollbar.size = _stats.GetHealth()/_stats.MaxHealth;
        }

        public override void Active()
        {
            throw new System.NotImplementedException();
        }

        public override void Inactive()
        {
            throw new System.NotImplementedException();
        }

        public override bool IsActive()
        {
            throw new System.NotImplementedException();
        }
    }
}