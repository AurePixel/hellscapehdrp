﻿using Hellscape.Entity.Player;
using UnityEngine;

namespace Hellscape.Managers
{
    [ExecuteInEditMode]
    public class CameraManager : MonoBehaviour
    {
        [SerializeField] private Player _player;
        [SerializeField] private Vector3 _offset = new Vector3(0f, 8.5f, -7f);
        [SerializeField, Range(0f, 360f)] private float _rotation = 68f;

        void LateUpdate()
        {
            transform.position = _player.transform.position + _offset;
            transform.rotation = Quaternion.Euler(_rotation, 0, 0);
        }
    }
}
