﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Hellscape.Managers
{
    public class SceneManager : MonoBehaviour
    {
        public static SceneManager Instance { get; private set; }

        private void Awake()
        {
            Instance = this;
        }

        public void LoadScene(GameState gameState)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(gameState.ToString());
        }
    }
}
