﻿using UnityEngine;

namespace Hellscape.Managers
{
    public class GameMasterManager : MonoBehaviour
    {
        private GameState _gameState;
        private SceneManager _sceneManager = SceneManager.Instance;
        public static GameMasterManager Instance { get; private set; }

        private void Awake()
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }

        public void UpdateGameState(GameState gameState)
        {
            _gameState = gameState;
            _sceneManager.LoadScene(_gameState);
            MusicManager.Instance.SwitchMusic();
        }

        public GameState GetGameState()
        {
            return _gameState;
        }
    }
}
