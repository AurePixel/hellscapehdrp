﻿using UnityEngine;

namespace Hellscape.Managers 
{
    public class MusicManager : MonoBehaviour 
    {
        [SerializeField] private AudioClip _mainTheme;
        [SerializeField] private AudioClip _inGameTheme;
        [SerializeField] private AudioClip _bossTheme;
        [SerializeField] private AudioSource _audioSource;

        private static MusicManager _instance;
    
        public static MusicManager Instance
        {
            get
            {
                if(_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<MusicManager>();
    
                    //Tell unity not to destroy this object when loading a new scene!
                    DontDestroyOnLoad(_instance.gameObject);
                }
    
                return _instance;
            }
        }
    
        private void Awake() 
        {
            if(_instance == null)
            {
                //If I am the first instance, make me the Singleton
                _instance = this;
                DontDestroyOnLoad(this);
            }
            else
            {
                //If a Singleton already exists and you find
                //another reference in scene, destroy it!
                if(this != _instance)
                    Destroy(this.gameObject);
            }
        }

        public void SwitchMusic()
        {
            var currentGameState = GameMasterManager.Instance.GetGameState();

            if (currentGameState == GameState.Loading || currentGameState == GameState.Start)
            {
                _audioSource.clip = _mainTheme;
            }
            else if (currentGameState == GameState.InGame)
            {
                _audioSource.clip = _inGameTheme;
            }
            else
            {
                _audioSource.Stop();
            }
        }
    }
}