﻿using Hellscape.Core.StateMachine;
using UnityEngine;

namespace Hellscape.Entity.SharedData
{
    public interface IEntityData
    {
        Rigidbody Rigidbody { get; }
        Animator Animator { get; }
        AudioSource AudioSource { get; }
        EntityStats Stats { get; }
        Vector3 MovementDir { get; }
        Vector3 LookDir { get; }
        GameObject Target { get; set; }
    }
}