﻿using UnityEngine;

namespace Hellscape.Entity.SharedData
{
    [CreateAssetMenu(fileName = "Stats", menuName = "Entity/Stats", order = 0)]
    public class EntityStats : ScriptableObject
    {
        public float MaxHealth;
        public float BaseDamage;

        private float _health;

        public void SetHealth(float amount)
        {
            _health = amount;
        }

        public float GetHealth()
        {
            return _health;
        }
    }
}