﻿using Hellscape.Entity.Fennec.FennecStates;
using UnityEngine;

namespace Hellscape.Entity.Fennec
{
    public class Fennec : Entity
    {
        private void Start()
        {
            SetState(GetState<SeekState>());
        }

        private void Update()
        {
            if (!IsAlive()) return;
            if (CurrentState is SeekState && GetState<DashAttackState>().CanBeUsed() && Target != null)
            {
                SetState(GetState<DashAttackState>());
            }
            else if (!(CurrentState is SeekState) && CurrentState.IsStateComplete)
            {
                SetState(GetState<SeekState>());
            }

            CurrentState.HandleUpdateLogic();
        }


        private void FixedUpdate()
        {
            if(!IsAlive()) return;
            CurrentState.HandlePhysicsLogic();
        }

        private void OnCollisionEnter(Collision collision)
        {
            if(IsAlive())CurrentState.CollisionEnter(collision);
        }
    }
}