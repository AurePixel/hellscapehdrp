﻿using Hellscape.Core;
using Hellscape.Core.Cooldown;
using Hellscape.Core.StateMachine;
using Hellscape.Entity.SharedData;
using UnityEngine;

namespace Hellscape.Entity.Fennec.FennecStates
{
    public class DashAttackState : EntityState
    {
        [Header("Dash Attack")]
        [SerializeField] private float _dashForce;
        [SerializeField] private float _impactForce;
        [SerializeField] private float _dashCooldown;
        [SerializeField] private float _dashChargingCooldown;
        [SerializeField] private float _dashDurationCooldown;
        [SerializeField] private float _dashStunCooldown;
        [SerializeField] private ParticleSystem _stunParticle;
        [SerializeField] private ParticleSystem _spottingParticle;

        public string TargetTag { get; } = CONSTANTS.PLAYER_TAG;
        public float DashForce { get => _dashForce; }
        public float ImpactForce { get => _impactForce; }
        public Cooldown DashCooldown { get; private set; }
        public Cooldown DashChargingCooldown { get; private set; }
        public Cooldown DashDurationCooldown { get; private set; }
        public Cooldown DashStunCooldown { get; private set; }

        private Vector3 direction => (_entityData.Target.transform.position - transform.position).normalized;
        private bool _hasCharged = false;

        public override void Init(IEntityData sharedStateData)
        {
            base.Init(sharedStateData);
            DashCooldown = new Cooldown(_dashCooldown);
            DashChargingCooldown = new Cooldown(_dashChargingCooldown);
            DashDurationCooldown = new Cooldown(_dashDurationCooldown);
            DashStunCooldown = new Cooldown(_dashStunCooldown);
        }

        public override void HandlePhysicsLogic()
        {
            // Charging dash sound ???

            if (DashChargingCooldown.isEnded && !_hasCharged)
            {
                _entityData.Rigidbody.AddForce(direction * DashForce, ForceMode.Impulse);
                //PlaySound(GetSound("Duck"));
                DashDurationCooldown.Start();
                _hasCharged = true;
            }

            if (DashDurationCooldown.isEnded && DashChargingCooldown.isEnded && DashStunCooldown.isEnded)
            {
                if(_entityData.Animator.GetBool("IsDashing"))EndDash();
                else
                {
                    _entityData.Animator.SetBool("IsStun",false);
                    _stunParticle.Stop();
                }
                IsStateComplete = true;
            }
        }

        private void EndDash()
        {
            _entityData.Rigidbody.velocity = Vector3.zero;
            _entityData.Animator.SetBool("IsDashing", false);
        }

        private void Stun()
        {
            _stunParticle.Play();
            EndDash();
            DashStunCooldown.Start();
            _entityData.Animator.SetBool("IsStun",true);
        }

        public override void HandleUpdateLogic()
        {
            return;
        }

        public override void OnStateEnter()
        {
            base.OnStateEnter();

            DashChargingCooldown.Start();
            _spottingParticle.Play();
            _hasCharged = false;
            PlaySound(_sounds[0]);
            _entityData.Animator.SetBool("IsDashing", true);
        }

        public override void OnStateExit()
        {
            DashCooldown.Start();
        }

        public override bool CanBeUsed()
        {
            return DashCooldown.isEnded;
        }

        public override void CollisionEnter(Collision collision)
        {
            if (!collision.gameObject.CompareTag(CONSTANTS.GROUND_TAG) && _hasCharged && DashStunCooldown.isEnded && _entityData.Animator.GetBool("IsDashing"))
            {
                DashDurationCooldown.Stop();
                Stun();
                //collision.gameObject.GetComponent<Rigidbody>().AddForce(direction * ImpactForce);

                if (collision.gameObject.CompareTag(TargetTag))
                {
                    Entity entity = collision.gameObject.GetComponent<Entity>();
                    ((Player.Player)entity).Stun();
                    entity.TakeDamage(_entityData.Stats.BaseDamage);
                }
            }
        }
    }
}
