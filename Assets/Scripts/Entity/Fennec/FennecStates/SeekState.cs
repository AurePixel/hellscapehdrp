﻿using Hellscape.Core;
using Hellscape.Core.Cooldown;
using Hellscape.Core.StateMachine;
using Hellscape.Entity.SharedData;
using Hellscape.Movement;
using SDG.Platform.Entities;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Hellscape.Entity.Fennec.FennecStates
{
    public class SeekState : EntityState
    {
        public readonly List<Direction> Directions = new List<Direction>
        {
            Direction.Top,
            Direction.Right,
            Direction.Left,
            Direction.Bottom
        };

        [Header("Movement")]
        [SerializeField] private float _speed;
        [SerializeField] private float _followSpeed;
        [SerializeField] private float _changeDirectionCooldown;

        [Header("Detection")]
        [SerializeField] private float _viewRadius;

        public string TargetTag { get; } = CONSTANTS.PLAYER_TAG;
        public float Speed { get => _speed; }
        public Cooldown ChangeDirectionCooldown { get; set; }

        private Vector3 _currentDirection = Vector3.forward;

        public override void Init(IEntityData entityData)
        {
            base.Init(entityData);
            ChangeDirectionCooldown = new Cooldown(_changeDirectionCooldown);
        }
        

        public override void HandlePhysicsLogic()
        {
            if (_entityData.Target != null)
            {
                MovementHelper.Look(transform, _entityData.Target.transform.position);
                MovementHelper.Move(_entityData.Rigidbody, transform.forward, _followSpeed);
            }
            else
            {
                MovementHelper.LookSmooth(transform, _currentDirection, 10);
                MovementHelper.Move(_entityData.Rigidbody, _currentDirection, Speed);
            }
        }

        public override void HandleUpdateLogic()
        {
            if(_entityData.Target) return;
            var randomDir = GetRandomDirection();
            Ray ray = new Ray(transform.position, _currentDirection);

            if (ChangeDirectionCooldown.isEnded || Physics.Raycast(ray, 5))
            {
                _currentDirection = randomDir;
                ChangeDirectionCooldown.Start();
            }
            FindTarget();
        }

        private void FindTarget()
        {
            Collider[] objectsInViewRadius = Physics.OverlapSphere(transform.position, _viewRadius);

            foreach (Collider collider in objectsInViewRadius)
            {
                if (collider.gameObject.CompareTag(TargetTag))
                {
                    _entityData.Target = collider.gameObject;
                    //PlaySound(GetSound("Duck"));
                    return;
                }

                _entityData.Target = null;
            }
        }

        private Vector3 GetRandomDirection()
        {
            var index = Random.Range(0, Directions.Count);
            Direction randomDirection = Directions[index];

            switch (randomDirection)
            {
                case Direction.Bottom:
                    return Vector3.back;
                case Direction.Top:
                    return Vector3.forward;
                case Direction.Left:
                    return Vector3.left;
                case Direction.Right:
                    return Vector3.right;
                default:
                    return Vector3.forward;
            }
        }

        // private void OnDrawGizmos()
        // {
        //     Gizmos.color = Color.green;
        //     Gizmos.DrawWireSphere(transform.position, _viewRadius);
        // }
    }
}
