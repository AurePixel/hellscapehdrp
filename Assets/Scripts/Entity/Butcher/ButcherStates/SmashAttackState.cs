﻿using Hellscape.Core.Cooldown;
using Hellscape.Entity.SharedData;
using UnityEngine;

namespace Hellscape.Entity.Butcher.ButcherStates
{
    public class SmashAttackState : EntityState
    {
        
        [Header("SmashAttack")]
        [SerializeField] private float _attackCooldown;
        [SerializeField] private GameObject _attackSmash;
        [SerializeField] private Transform _smashPoint;
        
        public Cooldown SmashAttackCooldown{ get; private set; }

        public override void Init(IEntityData sharedStateData)
        {
            base.Init(sharedStateData);
            SmashAttackCooldown = new Cooldown(_attackCooldown);
        }
        
        public override void OnStateEnter()
        {
            base.OnStateEnter();
            _entityData.Animator.SetBool("IsSmashing", true);
            //PlaySound(GetSound("Cry"));
            //PlaySound(GetSound("GroundTouch"));
            //PlaySound(GetSound("Vibration"));

        }

        public override void HandleUpdateLogic()
        {
            base.HandleUpdateLogic();
            //Si le temps actuel est plus petit que le début du coup + le cooldown
            //Si le cooldown du chargement de l'attack est passé par apport au début
            float animTime = _entityData.Animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
            if (_entityData.Animator.GetCurrentAnimatorStateInfo(0).IsName("HitSmash") && animTime >=0.2f)
            {
                PlaySound(_sounds[0]);
                AttackArea.AttackArea attack = Instantiate(_attackSmash, _smashPoint.position, Quaternion.identity).GetComponent<AttackArea.AttackArea>();
                attack.SetBaseDamage(_entityData.Stats.BaseDamage);
                SmashAttackCooldown.Start();
                IsStateComplete = true;
            }
        }

        public override void Interrupt()
        {
            base.Interrupt();
            SmashAttackCooldown.Start();
        }

        public override void OnStateExit()
        {
            _entityData.Animator.SetBool("IsSmashing", false);
        }

        public override bool CanBeUsed()
        {
            return SmashAttackCooldown.isEnded;
        }
    }
}
