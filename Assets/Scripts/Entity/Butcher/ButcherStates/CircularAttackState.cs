﻿using Hellscape.Core;
using Hellscape.Core.Cooldown;
using Hellscape.Core.StateMachine;
using Hellscape.Entity.SharedData;
using Hellscape.Movement;
using UnityEngine;

namespace Hellscape.Entity.Butcher.ButcherStates
{
    public class CircularAttackState : EntityState
    {
        [SerializeField] private float _speed;
        [SerializeField] private float _attackMultiplier;
        
        [Header("CircularAttack")]
        [SerializeField] private float _attackDuration;
        [SerializeField] private float _attackCooldown;
        [SerializeField] private float _attackRange;

        //Circular Attack
        public Cooldown CircularAttackDuration { get; private set; }
        public Cooldown CircularAttackCooldown { get; private set; }
        public float CircularAttackRange { get => _attackRange; }
        public float Speed { get => _speed; }


        public override void Init(IEntityData entityData)
        {
            base.Init(entityData);
            CircularAttackDuration = new Cooldown(_attackDuration);
            CircularAttackCooldown = new Cooldown(_attackCooldown);
        }
        
        public override void OnStateEnter()
        {
            base.OnStateEnter();
            CircularAttackDuration.Start();
            _entityData.Animator.SetBool("IsSpining", true);
            PlayLoopSound(_sounds[0]);
            //PlaySound(GetSound("Laught"));
            //PlaySound(GetSound("Chain"));
            //PlaySound(GetSound("AirRotation"));

            //TODO Activer le collider qui fera les dégats.
        }
        
        public override void HandlePhysicsLogic()
        {
            base.HandlePhysicsLogic();
            Vector3 targetPos = _entityData.Target.transform.position;
            targetPos.y = transform.position.y;
            MovementHelper.Look(transform, targetPos);
            MovementHelper.Move(_entityData.Rigidbody, _entityData.Target.transform, Speed, CircularAttackRange);
        }

        public override void HandleUpdateLogic()
        {
            base.HandleUpdateLogic();
            if (CircularAttackDuration.isEnded)
            {
                CircularAttackCooldown.Start();
                EndPlayLoopSound();
                IsStateComplete = true;
            }
        }

        public override void Interrupt()
        {
            base.Interrupt();
            CircularAttackCooldown.Start();
        }

        public override void OnStateExit()
        {
            _entityData.Animator.SetBool("IsSpining", false);
            //TODO Desactiver le collider qui fera les degats.
        }

        public override bool CanBeUsed()
        {
            return CircularAttackCooldown.isEnded;
        }
        
        public override void TriggerEnter(Collider other)
        {
            if(!other.CompareTag(CONSTANTS.PLAYER_TAG)) return;
            Entity entity = other.GetComponent<Entity>();
            if(entity) entity.TakeDamage(_entityData.Stats.BaseDamage * _attackMultiplier);
        }
    }
}
