﻿namespace Hellscape.Entity.Butcher.ButcherStates
{
    public enum Direction
    {
        Right, 
        Left,
        Top,
        Bottom
    }
}
