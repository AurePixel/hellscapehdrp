﻿using System.Collections.Generic;
using Hellscape.Core.Cooldown;
using Hellscape.Core.StateMachine;
using Hellscape.Entity.SharedData;
using Hellscape.Movement;
using UnityEngine;

namespace Hellscape.Entity.Butcher.ButcherStates
{
    public class MoveAroundState : EntityState
    {
        public readonly List<Direction> Directions = new List<Direction>
        {
            Direction.Top,
            Direction.Right,
            Direction.Left,
            Direction.Bottom
        };
        
        private Vector3 _currentDirection = Vector3.forward;
        
        [Header("Movement")]
        [SerializeField] private float _speed;
        [SerializeField] private float _changeDirectionCooldown;
        
        public float Speed{ get => _speed; }
        public Cooldown ChangeDirectionCooldown { get; set; }

        public override void Init(IEntityData sharedStateData)
        {
            base.Init(sharedStateData);
            ChangeDirectionCooldown = new Cooldown(_changeDirectionCooldown);
        }
        
        public override void OnStateEnter()
        {
            _entityData.Animator.SetBool("IsWalking", true);
            //PlaySound(GetSound("Walk"));
        }

        public override void HandlePhysicsLogic()
        {
            MovementHelper.LookSmooth(transform, _currentDirection, 10);

            if (!_entityData.Animator.GetCurrentAnimatorClipInfo(0)[0].clip.name.Equals("hit"))
                MovementHelper.Move(_entityData.Rigidbody, _currentDirection, Speed);
        }

        public override void HandleUpdateLogic()
        {
            var randomDir = GetRandomDirection();
            Ray ray = new Ray(transform.position, _currentDirection);
            int layer_mask = LayerMask.GetMask("Default");

            if (ChangeDirectionCooldown.isEnded || Physics.Raycast(ray, 5,layer_mask))
            {
                _currentDirection = randomDir;
                ChangeDirectionCooldown.Start();
            }
        }

        public override void OnStateExit()
        {
            _entityData.Animator.SetBool("IsWalking", false);
        }

        #region Private Methods
        private Vector3 GetRandomDirection()
        {
            var index = Random.Range(0, Directions.Count);
            Direction randomDirection = Directions[index];

            switch (randomDirection)
            {
                case Direction.Bottom:
                    return Vector3.back;
                case Direction.Top:
                    return Vector3.forward;
                case Direction.Left:
                    return Vector3.left;
                case Direction.Right:
                    return Vector3.right;
                default:
                    return Vector3.forward;
            }
        }
        #endregion


    }
}
