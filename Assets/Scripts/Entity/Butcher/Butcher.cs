﻿using Hellscape.Core;
using Hellscape.Entity.Butcher.ButcherStates;
using UnityEngine;

namespace Hellscape.Entity.Butcher
{
    public class Butcher : Entity
    {
        [SerializeField] private float _attackRange = 5f;

        private void Start()
        {
            SetState(GetState<MoveAroundState>());
        }

        // Update is called once per frame
        void Update()
        {
            if (!IsAlive())return;
            if (IsTargetAttackable() && CurrentState is MoveAroundState)
            {
                if (!IsBehind(Target) && GetState<SmashAttackState>().CanBeUsed())
                {
                    SetState(GetState<SmashAttackState>());
                }
                else if (IsBehind(Target) && GetState<CircularAttackState>().CanBeUsed())
                {
                    SetState(GetState<CircularAttackState>());
                }
            }
            //Si je n'ai rien pu faire d'autre et que je ne suis pas déja entrain de bouger, je bouge
            else if (!(CurrentState is MoveAroundState) && CurrentState.IsStateComplete)
            {
                SetState(GetState<MoveAroundState>());
            }

            CurrentState.HandleUpdateLogic();
        }

        private void FixedUpdate()
        {
            if(!IsAlive()) return;
            CurrentState.HandlePhysicsLogic();
        }

        private bool IsBehind(GameObject target)
        {
            var toTarget = (target.transform.position - transform.position).normalized;

            //Le dot product (produit scalaire) permet de connaitre la position d'un vecteur par rapport a un autre
            //Si il est nul, les deux vecteurs sont perpendiculaire
            //Si il est > 0, alors l'angle est plus petit que 90°
            //Si il est < 0, alors l'angle est plus grand que 90°
            //https://fr.wikipedia.org/wiki/Produit_scalaire

            var hitPos = Vector3.Dot(toTarget, transform.forward);

            return hitPos <= 0;
        }

        private bool IsTargetAttackable()
        {
            return Target != null && Vector3.Distance(Target.transform.position, transform.position) < _attackRange;
        }

        private void OnCollisionEnter(Collision collision)
        {
            if(!IsAlive()) return;
            if (!Target && collision.gameObject.CompareTag(CONSTANTS.PLAYER_TAG))
            {
                Target = collision.gameObject;
            }
        }
    }
}
