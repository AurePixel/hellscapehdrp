﻿namespace Hellscape.Entity.Lancer.LancerStates
{
    public enum Direction
    {
        Right, 
        Left,
        Top,
        Bottom
    }
}
