﻿using Hellscape.Core;
using Hellscape.Core.Cooldown;
using Hellscape.Core.StateMachine;
using Hellscape.Entity.SharedData;
using Hellscape.Movement;
using UnityEngine;

namespace Hellscape.Entity.Lancer.LancerStates
{
    public class LaunchAttackState : EntityState
    {
        [Header("Launch Attack")]
        [SerializeField] private GameObject _spear;
        [SerializeField] private GameObject _spearSpawnPosition;
        [SerializeField] private float _launchCooldown;

        public Cooldown LaunchCooldown { get; private set; }
        public GameObject Spear { get => _spear; }
        public GameObject SpearSpawnPosition { get => _spearSpawnPosition; }

        private bool _hasLaunched = false;

        public override void Init(IEntityData entityData)
        {
            base.Init(entityData);
            LaunchCooldown = new Cooldown(_launchCooldown);
        }
        
        public override void OnStateEnter()
        {
            base.OnStateEnter();
            _entityData.Animator.SetBool("IsThrowing", true);
            LaunchCooldown.Start();
            _hasLaunched = false;
        }
        
        public override void HandleUpdateLogic()
        {
            base.HandleUpdateLogic();
            float animTime = _entityData.Animator.GetCurrentAnimatorStateInfo(0).normalizedTime;

            if (_entityData.Animator.GetCurrentAnimatorStateInfo(0).IsName("ThrowSpear") && animTime >=0.5f && !_hasLaunched )
            {
                _hasLaunched = true;
                InstantiateSpear();
                PlaySound(_sounds[0]);
                IsStateComplete = true;
                _entityData.Animator.SetBool("IsThrowing", false);
            }
        }

        public override void HandlePhysicsLogic()
        {
            base.HandlePhysicsLogic();
            MovementHelper.Look(transform, _entityData.Target.transform.position);
            

        }

        private void InstantiateSpear()
        {
            GameObject spear = Instantiate(Spear, SpearSpawnPosition.transform.position, transform.rotation);
            spear.GetComponent<Spear>().Dammage = _entityData.Stats.BaseDamage;
            MovementHelper.Look(spear.transform, _entityData.Target.transform.position);
        }

        public override void OnStateExit()
        {
            _entityData.Animator.SetBool("IsThrowing", false);
        }

        public override bool CanBeUsed()
        {
            return LaunchCooldown.isEnded;
        }
        
        public override void CollisionEnter(Collision collision)
        {
        }
    }
}