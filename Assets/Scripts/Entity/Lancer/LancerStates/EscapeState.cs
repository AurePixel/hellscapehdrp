﻿using Hellscape.Core.Cooldown;
using Hellscape.Entity.SharedData;
using Hellscape.Movement;
using UnityEngine;

namespace Hellscape.Entity.Lancer.LancerStates
{
    public class EscapeState : EntityState
    {
        [Header("Escape")]
        [SerializeField] private float _speed;
        [SerializeField] private float _runCooldown;
        [SerializeField] private float _runDurationCooldown;
        [SerializeField] private float _rotationSpeed;

        public float Speed { get => _speed; }
        public Cooldown RunCooldown { get; private set; }
        public Cooldown RunDurationCooldown { get; private set; }
        public float RotationSpeed { get => _rotationSpeed; }
        
        public override void Init(IEntityData entityData)
        {
            base.Init(entityData);
            RunCooldown = new Cooldown(_runCooldown);
            RunDurationCooldown = new Cooldown(_runDurationCooldown);
        }
        
        public override void OnStateEnter()
        {
            base.OnStateEnter();
            RunDurationCooldown.Start();
            RunCooldown.Start();
            _entityData.Animator.SetBool("IsRunning", true);
        }
        
        public override void HandlePhysicsLogic()
        {
            base.HandlePhysicsLogic();;
            Vector3 direction = (transform.position - _entityData.Target.transform.position).normalized;

            if (!RunDurationCooldown.isEnded) {
                MovementHelper.LookSmooth(_entityData.Rigidbody.transform, direction, RotationSpeed);
                MovementHelper.Move(_entityData.Rigidbody, transform.forward, Speed);
            }
        }

        public override void HandleUpdateLogic()
        {
            base.HandleUpdateLogic();
            if (RunDurationCooldown.isEnded)
            {
                _entityData.Animator.SetBool("IsRunning", false);
                IsStateComplete = true;
            }
        }

        public override bool CanBeUsed()
        {
            return RunCooldown.isEnded;
        }
    }
}