﻿using Hellscape.Core;
using Hellscape.Entity.Lancer.LancerStates;
using UnityEngine;

namespace Hellscape.Entity.Lancer
{
    public class Lancer : Entity
    {
        [SerializeField] private float _attackRange = 10f;

        private void Start()
        {
            SetState(GetState<MoveAroundState>());
        }

        private void Update()
        {
            if(!IsAlive()) return;
            if (IsTargetAttackable() && GetState<LaunchAttackState>().CanBeUsed())
            {
                SetState(GetState<LaunchAttackState>());
            }
            CurrentState.HandleUpdateLogic();
        }

        private void FixedUpdate()
        {
            if(!IsAlive()) return;
            CurrentState.HandlePhysicsLogic();
        }

        private void OnCollisionEnter(Collision other)
        {
            if(!IsAlive() || !other.gameObject.CompareTag(CONSTANTS.PLAYER_TAG)) return;
            CurrentState.CollisionEnter(other);
            if (!(CurrentState is EscapeState))
            {
                SetState(GetState<EscapeState>());
            }
        }

        private void OnCollisionStay(Collision other)
        {
            if(!IsAlive() || !other.gameObject.CompareTag(CONSTANTS.PLAYER_TAG) ) return;
            CurrentState.CollisionStay(other);
            if (GetState<EscapeState>().CanBeUsed())
            {
                SetState(GetState<EscapeState>());
            }
        }

        private bool IsTargetAttackable()
        {
            return Target != null && Vector3.Distance(Target.transform.position, transform.position) < _attackRange;
        }
    }
}
