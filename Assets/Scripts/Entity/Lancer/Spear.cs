﻿using Hellscape.Core;
using Hellscape.Movement;
using UnityEngine;

namespace Hellscape.Entity.Lancer
{
    public class Spear : MonoBehaviour
    {
        [SerializeField] private Rigidbody _rb;
        [SerializeField] private float _speed;
        public float Dammage;
        
        void Update()
        {
            MovementHelper.Move(_rb, transform.forward, _speed);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(CONSTANTS.PLAYER_TAG))
            {
                Destroy(gameObject);
                Entity entity = other.GetComponent<Entity>();
                entity.TakeDamage(Dammage);
            }
        }
    }
}
