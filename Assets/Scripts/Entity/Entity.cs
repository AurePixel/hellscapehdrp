﻿using System.Collections.Generic;
using Hellscape.Core.Data;
using Hellscape.Core.StateMachine;
using Hellscape.Entity.SharedData;
using UnityEngine;

namespace Hellscape.Entity
{
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(AudioSource))]
    
    public class Entity : StateMachine, IEntityData
    {
        [SerializeField] protected List<AudioClip> _sounds;

        [SerializeField] private Animator _animator;
        [SerializeField] private Rigidbody _rigidbody;
        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private Collider _collider;
        [SerializeField] private EntityStats _stats;

        protected float _health
        {
            get => _stats.GetHealth();
            set => _stats.SetHealth(value);
        }
        
        public Rigidbody Rigidbody {
            get => _rigidbody;
            private set => _rigidbody = value;
        }
        public Animator Animator {
            get => _animator;
            private set => _animator = value;
        }
        public AudioSource AudioSource {
            get => _audioSource;
            private set => _audioSource = value;
        }
        
        public EntityStats Stats => _stats;
        
        public Vector3 MovementDir { get; protected set; }
        public Vector3 LookDir { get; protected set; }
        public GameObject Target { get; set; }


        protected virtual void Awake()
        {
            if (!Animator) Animator = GetComponent<Animator>();
            if (!Rigidbody) Rigidbody = GetComponent<Rigidbody>();
            if (!AudioSource) AudioSource = GetComponent<AudioSource>();
            
            _health = Stats.MaxHealth;
            foreach (State state in States)
            {
                ((EntityState)state).Init(this);
            }
            
        }

        protected bool IsAlive()
        {
            return _health > 0;
        }

        protected float GetCurrentHealth()
        {
            return _health;
        }
	    
        public virtual void TakeDamage(float damage)
        {
            _health -= damage;
            if (!IsAlive())
            {
                Death();
            }
        }

        protected virtual void Death()
        {
            _rigidbody.velocity = Vector3.zero;
            Rigidbody.isKinematic = true;
            _collider.enabled = false;
            Animator.SetTrigger("Death");
            InstantiateSoul();
        }

        protected virtual void InstantiateSoul()
        {
            Instantiate(Resources.Load("soul"), transform.position, Quaternion.identity);
        }

        // public Sound GetSound(string soundName)
        // {
        //     foreach (Sound s in _sounds)
        //     {
        //         if (s.name.Equals(soundName))
        //             return s;
        //     }
        //
        //     Debug.LogError("*** ERROR::SOUND::GETSOUND:Unable to find sound " + soundName);
        //     return null;
        // }

        public void PlaySound(AudioClip sound)
        {
            AudioSource.PlayOneShot(sound);
        }
    }
}