﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using UnityEditor.Hardware;
using UnityEngine;


namespace Hellscape.Entity.Pact
{
    [Serializable]
    public class Pact : MonoBehaviour
    {
        [SerializeField] private PactObject _pactObject;
        //[SerializeField] private GameObject[] _books;
        [SerializeField] private GameObject[] _pacts;
        [SerializeField] private Animator _pactAnimator;
        [SerializeField] private Player.Player _player;
        [SerializeField] private Animator _animator;
        [SerializeField] private Animator _waterAnimator;
        [SerializeField] private float _timeBetweenAnimation = 1.5f;
        private bool _isSelected;

        // Start is called before the first frame update
        void Start()
        {
            if (!_animator) _animator = GetComponent<Animator>();
            _pacts = GameObject.FindGameObjectsWithTag("Pact");
            _waterAnimator = GameObject.FindGameObjectWithTag("PactWater").GetComponent<Animator>();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                // le pacte se soulève et flotte
                OpenPact();
                _player = (Player.Player) other.gameObject.GetComponent<Entity>();
            }
        }
    
        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                ClosePact();
                _player = null;
            }
        }

        private void OnGUI()
        {
            if (Input.GetKeyDown(KeyCode.E) && _player)
            {
                //_player.SetPact(_pactObject);
                _isSelected = true;
                CloseAllPacts();
                _player = null;
            }
        }

        public void OpenPact()
        {
            // make animation
            _animator.SetBool("hover",true);
        }
    
        public void ClosePact()
        {
            // make animation
            _animator.SetBool("hover",false);
        }

        public IEnumerator CloseFinal()
        {
            if (_isSelected)
            {
                _pactAnimator.SetTrigger("close");
            }
            else
            {
                _pactAnimator.SetTrigger("select");
            }
            yield return new WaitForSeconds(_timeBetweenAnimation);
            _animator.SetTrigger("close");
            yield return new WaitForSeconds(_timeBetweenAnimation);
            Destroy(gameObject);
        }

        private void Close()
        {
            Destroy(this);
        }
        
        private void CloseAllPacts()
        {
            foreach (GameObject pact in _pacts)
            {
                StartCoroutine(pact.GetComponent<Pact>().CloseFinal());
                // le pacte rentre dans leur grimoire
                // if (transform.GetChild(0).gameObject != pact)
                // {
                //     pact.GetComponent<Animator>().SetTrigger("close");
                // }
                // else
                // {
                //     pact.GetComponent<Animator>().SetTrigger("select");
                // }
            }
            _waterAnimator.SetTrigger("spread");

            // foreach (GameObject book in _books)
            // {
            //     // fermeture des grimoires
            //     book.GetComponent<Animator>().SetTrigger("close");
            //     if (book != gameObject)
            //     {
            //         Destroy(book.GetComponent<Pact>());
            //     }
            // }
            //
            // yield return new WaitForSeconds(_timeBetweenAnimation);
            
            
        }
    }
}
