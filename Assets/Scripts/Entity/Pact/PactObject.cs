﻿using UnityEngine;

namespace Hellscape.Entity.Pact
{
    [CreateAssetMenu(fileName = "Pact", menuName = "Pact", order = 0)]
    public class PactObject : ScriptableObject
    {
        public PactType Type;
    }
}
