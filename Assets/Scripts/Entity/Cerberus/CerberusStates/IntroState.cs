﻿namespace Hellscape.Entity.Cerberus.CerberusStates
{
    public class IntroState: EntityState
    {
        public override void HandleUpdateLogic()
        {
            base.HandleUpdateLogic();
            float animTime = _entityData.Animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
            if (_entityData.Animator.GetCurrentAnimatorStateInfo(0).IsName("Intro") && animTime >=0.6f && animTime <0.61f)
            {
                PlaySound(_sounds[0]);
            }
            if (_entityData.Animator.GetCurrentAnimatorStateInfo(0).IsName("Intro") && animTime >=0.9f)
            {
                IsStateComplete = true;
            }
        }
    }
}