﻿using Hellscape.Core.Cooldown;
using Hellscape.Entity.SharedData;
using UnityEngine;

namespace Hellscape.Entity.Cerberus.CerberusStates
{
    public class WaitState: EntityState
    {
        [SerializeField] private float _waitTime;
        private Cooldown _waitTimeCooldown;

        public override void Init(IEntityData entityData)
        {
            base.Init(entityData);
            _waitTimeCooldown = new Cooldown(_waitTime);
        }

        public override void HandleUpdateLogic()
        {
            base.HandleUpdateLogic();
            if (_waitTimeCooldown.isEnded)
            {
                _waitTimeCooldown.Start();
                //shake camera
                //sound
            }
        }
    }
}