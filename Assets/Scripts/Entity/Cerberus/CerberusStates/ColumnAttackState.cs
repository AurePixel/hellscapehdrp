﻿using System.Collections.Generic;
using Hellscape.Core.Data;
using UnityEngine;

namespace Hellscape.Entity.Cerberus.CerberusStates
{
    public class ColumnAttackState: EntityState
    {
        [SerializeField] private GameObject _attackFireColumn;
        [SerializeField] private ParticleSystem _shockWave;
        private List<GameObject> _fireColumns = new List<GameObject>();
        [SerializeField] private Sound _attackSound;

        public override void OnStateEnter()
        {
            base.OnStateEnter();
            //play anim
            _entityData.Animator.SetTrigger("AttackColumn");
            //play sound
        }

        public override void HandleUpdateLogic()
        {
            base.HandleUpdateLogic();
            float animTime = _entityData.Animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
            if (_entityData.Animator.GetCurrentAnimatorStateInfo(0).IsName("ColumnAttack") && animTime >=0.9f)
            {
                Attack();
            }
        }

        private void Attack()
        {
            Vector3 pos = _entityData.Target.transform.position;
            pos.y = pos.y - 1;
            GameObject column = Instantiate(_attackFireColumn, pos, Quaternion.identity);
            PlaySound(_sounds[0]);
            _fireColumns.Add(column);
            _shockWave.Play();
            IsStateComplete = true;
            
            
        }
        public void ResetState()
        {
            foreach (GameObject column in _fireColumns)
            {
                Destroy(column);
            }
        }
    }
}