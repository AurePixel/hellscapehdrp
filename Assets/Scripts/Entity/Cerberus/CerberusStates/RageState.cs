﻿using UnityEngine;

namespace Hellscape.Entity.Cerberus.CerberusStates
{
    public class RageState : EntityState
    {
        [SerializeField] private Material _rageMat;
        public override void OnStateEnter()
        {
            base.OnStateEnter();
            _entityData.Animator.SetTrigger("Rage");
            _entityData.Animator.SetFloat("IdleNb",1);
            //glow
        }

        public override void HandleUpdateLogic()
        {
            base.HandleUpdateLogic();
            float animTime = _entityData.Animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
            if (_entityData.Animator.GetCurrentAnimatorStateInfo(0).IsName("Rage") && animTime >=0.9f)
            {
                //freshnel
                _rageMat.SetFloat("Vector1_31B444DF",2.5f);
                //rage power
                _rageMat.SetFloat("Vector1_91DD580F",15f);
                //rage on
                _rageMat.SetFloat("Vector1_66CA59F4",0.3f);
                IsStateComplete = true;
            }
        }
    }
}