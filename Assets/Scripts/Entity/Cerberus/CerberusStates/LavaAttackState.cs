﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Hellscape.Core.Cooldown;
using Hellscape.Entity.SharedData;
using Hellscape.Rooms.CerberusRoom;
using UnityEngine;

namespace Hellscape.Entity.Cerberus.CerberusStates
{
    public class LavaAttackState : EntityState
    {
        [SerializeField] private List<HeadArea> _heads;
        [SerializeField] private float _waitTime;
        [SerializeField] ParticleSystem _scream;
        private Cooldown _waitAttackCooldown;
        private float _headsActivated = 0;
        private bool _hasAttacked;
        
        public override void Init(IEntityData sharedStateData)
        {
            base.Init(sharedStateData);
            _waitAttackCooldown = new Cooldown(_waitTime);
            if (_heads.Count < 1) _heads = FindObjectsOfType<HeadArea>().ToList();
        }
        
        public override void OnStateEnter()
        {
            base.OnStateEnter();
            _waitAttackCooldown.Start();
            _hasAttacked = false;
            //eyes glow
        }
        
        public override void HandleUpdateLogic()
        {
            float animTime = _entityData.Animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
            if (_waitAttackCooldown.isEnded && !_hasAttacked)
            {
                Attack();
                return;
            }
            if (_entityData.Animator.GetCurrentAnimatorStateInfo(0).IsName("LavaAttack") && animTime >=0.1f && animTime <0.11f)
            {
                Debug.Log("play");
                _scream.Play();
                PlaySound(_sounds[0]);
            }
            if (_entityData.Animator.GetCurrentAnimatorStateInfo(0).IsName("LavaAttack") && animTime >=0.5f)
            {
                IsStateComplete = true;
            }
        }
        
        private void Attack()
        {
            //find the right Head
            HeadArea targetHead = _heads.Find(head => head.HasPlayer());
            if(!targetHead) return;
            if(targetHead.isActivated()) return;
            
            targetHead.ActivateArea();
            _hasAttacked = true;
            //start anim
            _entityData.Animator.SetTrigger("AttackLava");
            //play sound
        }

        public void ResetState()
        {
            foreach (HeadArea head in _heads)
            {
                head.DesactivateArea();
            }
        }

        public override bool CanBeUsed()
        {
            return _headsActivated < _heads.Count;
        }
    }
}