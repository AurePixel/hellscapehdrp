﻿using System;
using Hellscape.Core;
using Hellscape.Core.Cooldown;
using Hellscape.Core.StateMachine;
using Hellscape.Entity.Cerberus.CerberusStates;
using Hellscape.Movement;
using UnityEngine;

namespace Hellscape.Entity.Cerberus
{
    public class Cerberus : Entity
    {
        [SerializeField] private float _timeBetweenAttacks;
        [SerializeField] private Material _mat;
        private Cooldown _betweenAttacksCooldown;
        private int _currentPhase = 1;
        private float[] _phaseSteps;
        private State _nextAttack;
        private bool isAttacking;
        
        private void Start()
        {
            Target = GameObject.FindWithTag(CONSTANTS.PLAYER_TAG);
            _phaseSteps = new[] {Stats.MaxHealth, Stats.MaxHealth * 0.65f, Stats.MaxHealth * 0.3f};
            _betweenAttacksCooldown = new Cooldown(_timeBetweenAttacks);
            _nextAttack = GetNextAttack();
            SetState(GetState<IntroState>());
            isAttacking = true;
            //freshnel
            _mat.SetFloat("Vector1_31B444DF",0f);
            //rage power
            _mat.SetFloat("Vector1_91DD580F",0f);
            //rage on
            _mat.SetFloat("Vector1_66CA59F4",0f);
        }

        private void Update()
        {
            if (CurrentState.IsStateComplete)
            {
                _nextAttack = GetNextAttack();
                SetState(GetState<WaitState>());
                _betweenAttacksCooldown.Start();
                isAttacking = false;
            }
            else if (_betweenAttacksCooldown.isEnded && !isAttacking)
            {
                SetState(_nextAttack);
                isAttacking = true;
            }
            
            CurrentState.HandleUpdateLogic();
        }

        private void FixedUpdate()
        {
            if(Target) MovementHelper.Look(transform,Target.transform.position);
        }

        public override void TakeDamage(float damage)
        {
            base.TakeDamage(damage);
            if (_currentPhase<_phaseSteps.Length && GetCurrentHealth() < _phaseSteps[_currentPhase]) NextPhase();
        }

        private void NextPhase()
        {
            SetState(GetState<WaitState>());
            CurrentState.Interrupt();
            GetState<LavaAttackState>().ResetState();
            GetState<ColumnAttackState>().ResetState();
            _currentPhase++;
            if (_currentPhase == 3)
            {
                CurrentState.Interrupt();
                SetState(GetState<RageState>());
            }
        }

        private State GetNextAttack()
        {
            State nextAttack = GetState<WaitState>();
            switch (_currentPhase)
            {
                case 1:
                    if(GetState<LavaAttackState>().CanBeUsed())nextAttack = GetState<LavaAttackState>();
                    break;
                case 2:
                    nextAttack = GetState<ColumnAttackState>();
                    break;
                case 3:
                    if (!(CurrentState is LavaAttackState) && GetState<LavaAttackState>().CanBeUsed())
                    {
                        nextAttack = GetState<LavaAttackState>();
                    }
                    else
                    {
                        nextAttack = GetState<ColumnAttackState>();
                    }
                    break;
            }
            return nextAttack;
        }

        protected override void Death()
        {
            InstantiateSoul();
            Destroy(gameObject);
        }

        protected override void InstantiateSoul()
        {
            Instantiate(Resources.Load("soulCerberus"), transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}