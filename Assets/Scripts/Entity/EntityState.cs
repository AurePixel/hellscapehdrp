﻿using System.Collections.Generic;
using Hellscape.Core.Data;
using Hellscape.Core.StateMachine;
using Hellscape.Entity.SharedData;
using UnityEngine;

namespace Hellscape.Entity
{
    public class EntityState : State
    {
        protected IEntityData _entityData;
        [SerializeField] protected List<AudioClip> _sounds;

        public virtual void Init(IEntityData entityData)
        {
            _entityData = entityData;
        }
        
        public override void HandleUpdateLogic()
        {
            if(IsStateComplete) return;
        }

        public override void HandlePhysicsLogic()
        {
            if(IsStateComplete) return;
        }
        
        public override void Interrupt()
        {
            IsStateComplete = true;
        }
        
        // public Sound GetSound(string soundName)
        // {
        //     foreach (Sound s in _sounds)
        //     {
        //         if (s.name.Equals(soundName))
        //             return s;
        //     }
        //
        //     Debug.LogError("*** ERROR::SOUND::GETSOUND:Unable to find sound " + soundName);
        //     return null;
        // }
        public void PlayLoopSound(AudioClip sound)
        {
            _entityData.AudioSource.clip = sound;
            _entityData.AudioSource.loop = true;
            _entityData.AudioSource.Play();
        }

        public void EndPlayLoopSound()
        {
            _entityData.AudioSource.Stop();
            _entityData.AudioSource.clip = null;
            _entityData.AudioSource.loop = true;
        }

        public void PlaySound(AudioClip sound)
        {
            //_entityData.AudioSource.volume = sound.Volume;
            _entityData.AudioSource.PlayOneShot(sound);
        }
    }
}