﻿using System;
using System.Collections;
using Hellscape.Core;
using Hellscape.Entity.Pact;
using Hellscape.Entity.Player.PlayerStates;
using Hellscape.Soul;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

namespace Hellscape.Entity.Player
{
    
    public class Player: Entity
    {
        private float _distToGround;
        private bool _isGrounded;
        [SerializeField] private PlayerAttribute _playerAttribute;
        
        protected override void Awake()
        {
            base.Awake();
            _distToGround = GetComponent<Collider>().bounds.extents.y;
            _playerAttribute = ScriptableObject.CreateInstance<PlayerAttribute>();
        }

        private void Start()
        {
            SetState(GetState<MovementState>());
        }

        private void Update()
        {
            if(!IsAlive()) return;

            if (CurrentState.IsStateComplete && !(CurrentState is MovementState))
            {
                Debug.Log(CurrentState);
                SetState(GetState<MovementState>());
            }

            if (IsGrounded() && !(CurrentState is StunState))
            {
                MovementDir = new Vector3(Input.GetAxisRaw("Horizontal"),0,Input.GetAxisRaw("Vertical"));
                LookDir = (MovementDir.magnitude > Vector3.zero.magnitude)? MovementDir : LookDir;
                if (Input.GetButtonDown("Attack"))
                {
                    if (!(CurrentState is AttackState))
                    {
                        SetState(GetState<AttackState>());
                    }else if (CurrentState.CanBeUsed())
                    {
                        ((AttackState)CurrentState).NextAttack();
                    }
                }
                else if (Input.GetButtonDown("AttackSpecial") && (CurrentState is AttackState) && CurrentState.CanBeUsed())
                {
                    ((AttackState) CurrentState).NextAttack(true);
                }
                else if(Input.GetButtonDown("Dash") && GetState<DashState>().CanBeUsed())
                {
                    SetState(GetState<DashState>());
                }
            }

            CurrentState.HandleUpdateLogic();
        }
        
        private void FixedUpdate()
        {
            if(!IsAlive()) return;
            CurrentState.HandlePhysicsLogic();
        }

        private bool IsGrounded ()
        { 
            return Physics.Raycast(transform.position, Vector3.down, _distToGround + 0.1f);
        }

        public void Stun()
        {
            
            if (GetState<StunState>().CanBeUsed() && IsAlive() && !(CurrentState is JumpState))
            {
                CurrentState.Interrupt();
                SetState(GetState<StunState>());
            }
        }

        public void Jump()
        {
            if (!(CurrentState is JumpState) && IsAlive() && !(CurrentState is StunState))
            {
                CurrentState.Interrupt();
                SetState(GetState<JumpState>());
            }
        }

        public void SetPact(PactObject pact)
        {
            //TODO : think of other possibilities
            _playerAttribute.SetPact(pact);
            switch (pact.Type)
            {
                case PactType.Fight:
                case PactType.Soul:
                case PactType.Sword:
                case PactType.Statistics:
                    Stats.MaxHealth += Random.Range(Stats.MaxHealth / 10, Stats.MaxHealth / 2);
                    _health = Stats.MaxHealth;
                    break;
            }
        }

        public void AddSouls(SoulType _soulType, SoulInfo _info)
        {
            switch (_soulType)
            {
                case SoulType.Minion:
                    _playerAttribute.AddSouls(1);
                    break;
                case SoulType.Boss:
                    _playerAttribute.AddSuperSouls(_info);
                    break;
            }
        }

        public override void TakeDamage(float damage)
        {
            base.TakeDamage(damage);
            PlaySound(_sounds[0]);
        }

        protected override void Death()
        {
            base.Death();
            StartCoroutine("OnDeath");
        }

        IEnumerator OnDeath()
        {
            PlayerPrefs.SetInt(CONSTANTS.SOULS_COUNTER, _playerAttribute.GetSouls());
            yield return new WaitForSeconds(3);
            SceneManager.LoadScene("EndGame");
        }

        private void OnTriggerEnter(Collider other)
        {
            if(IsAlive())CurrentState.TriggerEnter(other);
        }

        private void OnCollisionEnter(Collision other)
        {
            Rigidbody.velocity = Vector3.zero;
            if(IsAlive())CurrentState.CollisionEnter(other);
        }

        protected override void InstantiateSoul()
        {
            
        }
    }
}