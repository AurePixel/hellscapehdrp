﻿using System;
using System.Collections.Generic;
using Hellscape.Core;
using Hellscape.Entity.Pact;
using Hellscape.Soul;
using UnityEngine;


// life and nbre d'ames 
namespace Hellscape.Entity.Player
{
    [CreateAssetMenu(fileName = "PlayerAttribute", menuName = "Entity/Player/Attribute")]
    public class PlayerAttribute : ScriptableObject
    {
        private int _souls;
        [SerializeField] private List<SoulInfo> _superSouls;
        [SerializeField] private PactType? _pact; // one pacte -> if one chose the others are deactivated
            
            // manage souls
        public void AddSouls(int amount)
        {
            PlayerPrefs.SetInt(CONSTANTS.SOULS_COUNTER,PlayerPrefs.GetInt(CONSTANTS.SOULS_COUNTER) + amount);
            _souls += amount;
        }

        public int GetSouls()
        {
            if (!PlayerPrefs.HasKey(CONSTANTS.SOULS_COUNTER))
            {
                PlayerPrefs.SetInt(CONSTANTS.SOULS_COUNTER,0);
            }
            return PlayerPrefs.GetInt(CONSTANTS.SOULS_COUNTER);
        }

        public void ResetSouls()
        {
            _souls = 0;
        }
        
        // manage super souls
        public void AddSuperSouls(SoulInfo _soul)
        {
            _superSouls.Add(_soul);
        }

        public void ResetSuperSouls()
        {
            _superSouls.Clear();
        }
        
        // manage pactes
        public void SetPact(PactObject pactSelected)
        {
            _pact = pactSelected.Type;
        }

        public PactType? GetPact()
        {
            return _pact;
        }
            
        public void ResetPacts()
        {
            _pact = null;
        }
    }
}

