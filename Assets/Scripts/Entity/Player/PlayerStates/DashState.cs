﻿using Hellscape.Core;
using Hellscape.Core.Cooldown;
using Hellscape.Core.StateMachine;
using Hellscape.Entity.SharedData;
using Hellscape.Movement;
using UnityEngine;

namespace Hellscape.Entity.Player.PlayerStates
{
    public class DashState : EntityState
    {
        [Header("Dash")]
        [SerializeField] private float _speed;
        [SerializeField] private float _cooldown;
        [SerializeField] private float _duration;
        [SerializeField] private GameObject _dashTrail;
        public float DashSpeed => _speed;
        public Vector3 DashDir { get; set; }
        public Cooldown DashCooldown { get; private set; }
        public Cooldown DashDuration { get; private set; }
        

        public override void Init(IEntityData entityData)
        {
            base.Init(entityData);
            DashCooldown = new Cooldown(_cooldown);
            DashDuration = new Cooldown(_duration);
        }

        public override void OnStateEnter()
        {
            base.OnStateEnter();
            DashDuration.Start();
            _dashTrail.SetActive(true);
            _entityData.Animator.SetBool("IsDashing", true);
            DashDir = (_entityData.MovementDir == Vector3.zero)? transform.forward : _entityData.MovementDir;
            PlaySound(_sounds[0]);
        }

        public override void HandleUpdateLogic()
        {
            base.HandleUpdateLogic();
            //DashDir = (_entityData.MovementDir == Vector3.zero)? transform.forward : _entityData.MovementDir;
            if (DashDuration.isEnded)
            {
                _dashTrail.SetActive(false);
                IsStateComplete = true;
            }
        }

        public override void HandlePhysicsLogic()
        {
            base.HandlePhysicsLogic();
            MovementHelper.Move(_entityData.Rigidbody, DashDir, DashSpeed);
        }
        
        public override void OnStateExit()
        {
            DashCooldown.Start();
            _dashTrail.SetActive(false);
            _entityData.Rigidbody.velocity= Vector3.zero;
            _entityData.Animator.SetBool("IsDashing", false);
        }

        public override bool CanBeUsed()
        {
            return DashCooldown.isEnded;
        }

        public override void CollisionEnter(Collision collision)
        {
            if (!collision.gameObject.CompareTag(CONSTANTS.GROUND_TAG))
            {
                Interrupt();
            }
        }
    }
}