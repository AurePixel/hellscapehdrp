﻿using Hellscape.Core;
using Hellscape.Core.Cooldown;
using Hellscape.Core.StateMachine;
using Hellscape.Entity.SharedData;
using Hellscape.Movement;
using UnityEngine;

namespace Hellscape.Entity.Player.PlayerStates
{
    public class StunState : EntityState
    {
        [SerializeField] private float _cooldown;
        [SerializeField] private float _duration;
        public Cooldown StunCooldown { get; private set; }
        public Cooldown StunDuration { get; private set; }
        

        public override void Init(IEntityData entityData)
        {
            base.Init(entityData);
            StunCooldown = new Cooldown(_cooldown);
            StunDuration = new Cooldown(_duration);
        }

        public override void OnStateEnter()
        {
            base.OnStateEnter();
            StunDuration.Start();
            _entityData.Animator.SetBool("IsStun",true);
        }

        public override void HandleUpdateLogic()
        {
            base.HandleUpdateLogic();
            if (StunDuration.isEnded)
            {
                IsStateComplete = true;
            }
        }
        
        
        public override void OnStateExit()
        {
            _entityData.Animator.SetBool("IsStun", false);
            StunCooldown.Start();
        }

        public override bool CanBeUsed()
        {
            return StunCooldown.isEnded;
        }

        public override void CollisionEnter(Collision collision)
        {
        }
    }
}