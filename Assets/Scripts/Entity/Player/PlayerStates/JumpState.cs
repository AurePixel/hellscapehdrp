﻿using Hellscape.Core;
using Hellscape.Core.Cooldown;
using Hellscape.Core.StateMachine;
using Hellscape.Entity.SharedData;
using Hellscape.Movement;
using UnityEngine;

namespace Hellscape.Entity.Player.PlayerStates
{
    public class JumpState : EntityState
    {
        [SerializeField] private float _jumpSpeed;
        private bool _isFalling;

        public override void Init(IEntityData entityData)
        {
            base.Init(entityData);
        }

        public override void OnStateEnter()
        {
            base.OnStateEnter();
            _isFalling = false;
            _entityData.Rigidbody.AddForce(Vector3.up * _jumpSpeed , ForceMode.Impulse);
            _entityData.Animator.SetBool("IsJumping",true);
        }

        public override void HandleUpdateLogic()
        {
            base.HandleUpdateLogic();
            
            if (_entityData.Rigidbody.velocity.y <0 && !_isFalling)
            {
                _isFalling = true;
            }

            if (_isFalling && IsGrounded())
            {
                Debug.Log("is grounded");
                IsStateComplete = true;
            }
        }
        
        private bool IsGrounded ()
        { 
            return Physics.Raycast(transform.position, Vector3.down,  0.1f);
        }

        public override void OnStateExit()
        {
            _entityData.Animator.SetBool("IsJumping",false);
        }

        public override void CollisionEnter(Collision collision)
        {
            if (_isFalling && collision.gameObject.CompareTag(CONSTANTS.GROUND_TAG))
            {
                IsStateComplete = true;
            }
        }
    }
}