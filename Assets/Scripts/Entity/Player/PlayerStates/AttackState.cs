﻿using System;
using System.Collections;
using Hellscape.Core;
using Hellscape.Core.Data;
using Hellscape.Entity.SharedData;
using UnityEngine;

namespace Hellscape.Entity.Player.PlayerStates
{
    public class AttackState : EntityState
    {
        [SerializeField] private AttackData[] _attackDatas;
        [SerializeField] private GameObject _swordAttackTrail;
        [SerializeField] private GameObject _attackSpecialPrefab;
        private int _attackNb = 0;
        private int _nextAttackNb;
        private bool _nextAttackSpecial;
        private bool _attackSpecialRan;
        private bool _attackSpecialInstantiate;
        private AttackData _currentAttack;

        private float _maxTime = 0.5f;
        private float _minTime = 0.1f;
        public override void Init(IEntityData entityData)
        {
            base.Init(entityData);
        }
        
        public override void OnStateEnter()
        {
            base.OnStateEnter();
            _attackNb = 0;
            _nextAttackNb = 0;
            Attack();
            SetIsAttacking(true);
        }

        public override void HandleUpdateLogic()
        {
            base.HandleUpdateLogic();
            if (_entityData.Animator.GetCurrentAnimatorStateInfo(0).IsName("attack" + _attackNb) && !_attackSpecialRan)
            {
                float animTime= _entityData.Animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
                if (animTime > _maxTime)
                {
                    if (_nextAttackNb > _attackNb && !_nextAttackSpecial)
                    {
                        Attack();
                    }
                    else if (_nextAttackSpecial)
                    {
                        SetIsDoingSpecialAttack(true);
                    }
                    else if (animTime > 0.9f)
                    {
                        IsStateComplete = true;
                    }
                }
            }else if (_entityData.Animator.GetCurrentAnimatorStateInfo(0).IsName("AttackSpecial"))
            {
                float animTime= _entityData.Animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
                if (animTime > 0.3f && !_attackSpecialInstantiate)
                {
                    SpecialAttack();
                }
                else if (animTime > 0.9f)
                {
                    IsStateComplete = true;
                }
            }
        }

        private void SpecialAttack()
        {
            _attackSpecialInstantiate = true;
            Vector3 pos = transform.position;
            pos.y = 0.2f;
            AttackArea.AttackArea obj = Instantiate(_attackSpecialPrefab,pos, Quaternion.identity).GetComponent<AttackArea.AttackArea>();
            obj.SetBaseDamage(_entityData.Stats.BaseDamage);
        }
        
        private void Attack()
        {
            _attackNb = _nextAttackNb;
            _currentAttack = _attackDatas[_attackNb];
            _entityData.Animator.SetInteger("AttackNb",_attackNb);
            PlaySound(_sounds[0]);
        }

        public void NextAttack(bool isSpecial = false)
        {
            if (isSpecial)
            {
                _nextAttackSpecial = true;
            }
            else if (_nextAttackNb < _attackDatas.Length-1)
            {
                _nextAttackNb = _attackNb + 1;
            }
        }

        public override void OnStateExit()
        {
            base.OnStateExit();
            SetIsAttacking(false);
            _nextAttackSpecial = false;
            _attackSpecialInstantiate = false;
            SetIsDoingSpecialAttack(false);
            
        }

        public override bool CanBeUsed()
        {
            float attackTime = _entityData.Animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
            return attackTime > _minTime && attackTime < _maxTime && _nextAttackNb <= _attackNb && !_nextAttackSpecial;
        }

        private void SetIsDoingSpecialAttack(bool value)
        {
            _currentAttack = null;
            _attackSpecialRan = value;
            _entityData.Animator.SetBool("AttackSpecial",value);
            PlaySound(_sounds[0]);
        }
        
        private void SetIsAttacking(bool value)
        {
            _swordAttackTrail.SetActive(value);
            _entityData.Animator.SetBool("IsAttacking",value);
        }

        public override void TriggerEnter(Collider other)
        {
            if (!other.CompareTag(CONSTANTS.ENEMY_TAG)) return;
            Entity entity = other.GetComponent<Entity>();
            if(entity && _currentAttack) entity.TakeDamage(_entityData.Stats.BaseDamage * _currentAttack.AttackMultiplier);

        }
    }
}