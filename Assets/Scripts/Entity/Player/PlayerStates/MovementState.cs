﻿using Hellscape.Core.StateMachine;
using Hellscape.Entity.SharedData;
using Hellscape.Movement;
using UnityEngine;

namespace Hellscape.Entity.Player.PlayerStates
{
    public class MovementState : EntityState
    {
        [SerializeField] private float _speed;
        [SerializeField] private float _rotationSpeed;

        //MOVEMENT
        public float Speed => _speed;
        public float RotationSpeed => _rotationSpeed;

        private Vector3 _lastPos;
        

        public override void HandlePhysicsLogic()
        {
            Debug.Log("hello");
            Vector3 pos = transform.position;
            Vector3 vel = (pos - _lastPos) / Time.deltaTime;
            _lastPos = pos;
            if(_entityData.LookDir !=  Vector3.zero) transform.rotation = Quaternion.LookRotation(_entityData.LookDir);
            //MovementHelper.LookSmooth(transform, _entityData.LookDir, RotationSpeed);
            MovementHelper.Move(_entityData.Rigidbody, _entityData.MovementDir, Speed);
            _entityData.Animator.SetFloat("Velocity", vel.magnitude);
        }
    }
}