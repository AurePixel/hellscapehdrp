﻿namespace Hellscape
{
    public enum GameState
    {
        Start,
        TalentTree,
        Loading,
        InGame,
        EndGame
    }
}