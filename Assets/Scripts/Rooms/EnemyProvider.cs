﻿using System.Collections.Generic;
using UnityEngine;

namespace Hellscape.Rooms
{
    [CreateAssetMenu(fileName = "EnemyProvider", menuName = "Provider/Enemies")]
    public class EnemyProvider : ScriptableObject
    {
        [SerializeField] private List<GameObject> _enemies;

        public List<GameObject> GetRandomEnemies(int number)
        {
            List<GameObject> randomEnemies = new List<GameObject>();
            for (int i = 0; i < number; i++)
            {
                var enemyNumber = Random.Range(0, _enemies.Count);
                randomEnemies.Add(_enemies[enemyNumber]);
            }
            return randomEnemies;
        } 
    }
}
