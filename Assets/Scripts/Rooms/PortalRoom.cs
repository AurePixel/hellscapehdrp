﻿using Hellscape.Rooms.Dungeon;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Hellscape.Rooms.EnemyRoom
{
    public class PortalRoom : DungeonRoom
    {
        private const string CERBERUS_SCENE = "Cerbere";
        private const string PLAYER = "Player";

        protected override void Initialize()
        {
            throw new System.NotImplementedException();
        }

        private void OnCollisionEnter(Collision other) 
        {
            if (other.gameObject.CompareTag(PLAYER))
                SceneManager.LoadScene(CERBERUS_SCENE);
        }
    }
}