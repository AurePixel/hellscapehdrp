﻿using System.Collections.Generic;
using UnityEngine;

namespace Hellscape.Rooms.PacteRoom
{
    [CreateAssetMenu(fileName = "PactProvider", menuName = "Provider/Pact")]
    public class PactProvider : ScriptableObject
    {
        [SerializeField] private List<GameObject> _pacts;
        [SerializeField] private List<GameObject> tempPacts;

        public List<GameObject> GetRandomPacts(int number)
        {
            tempPacts.Clear();
            tempPacts.AddRange(_pacts);
            List<GameObject> randomPacts = new List<GameObject>();
            for (int i = 0; i < number; i++)
            {
                var pactNumber = Random.Range(0, tempPacts.Count);
                randomPacts.Add(tempPacts[pactNumber]);
                tempPacts.Remove(tempPacts[pactNumber]);
            }
            tempPacts.Clear();
            return randomPacts;
        }
    }
}
