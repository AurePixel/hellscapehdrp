﻿using System;
using Hellscape.Rooms.Dungeon;
using UnityEngine;

namespace Hellscape.Rooms.PacteRoom
{
    public class PactRoom : DungeonRoom
    {
        [SerializeField] private Transform _parent;
        [SerializeField] private PactProvider _pactProvider;
        [SerializeField] private GameObject[] _pos;

        protected override void Initialize()
        {
            var pacts = _pactProvider.GetRandomPacts(_pos.Length);

            for (int i = 0; i < _pos.Length; i++)
            {
                Instantiate(pacts[i], _pos[i].transform.position, _parent.rotation, _parent);
            }
        }

        void Start()
        {
            Initialize();
        }
    }
}

