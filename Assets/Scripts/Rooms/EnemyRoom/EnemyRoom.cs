﻿using Hellscape.Rooms.Dungeon;
using UnityEngine;

namespace Hellscape.Rooms.EnemyRoom
{
    public class EnemyRoom : DungeonRoom
    {
        [SerializeField] private Transform _parent;
        [SerializeField] private EnemyProvider _enemyProvider;
        [SerializeField] private GameObject[] _pos;

        protected override void Initialize()
        {
            var enemies = _enemyProvider.GetRandomEnemies(_pos.Length);

            for (int i = 0; i < _pos.Length; i++)
            {
                Instantiate(enemies[i], _pos[i].transform.position, Quaternion.identity, _parent);
            }

        }

        void Start()
        {
            Initialize();
        }
    }
}

