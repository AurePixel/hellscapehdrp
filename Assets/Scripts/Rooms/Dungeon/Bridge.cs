﻿using SDG.Platform.Entities;
using UnityEngine;

namespace Hellscape.Rooms.Dungeon
{
    public class Bridge : MonoBehaviour
    {
        [SerializeField] private Direction _direction;
        [SerializeField] private GameObject _replacementWall;

        public Direction GetDirection()
        {
            return _direction;
        }

        public void ActivateWall(bool isActivate)
        {
            _replacementWall.SetActive(isActivate);
        }
    }
}
