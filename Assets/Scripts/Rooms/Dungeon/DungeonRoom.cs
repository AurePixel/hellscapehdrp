﻿using System.Collections.Generic;
using UnityEngine;

namespace Hellscape.Rooms.Dungeon
{
    public abstract class DungeonRoom : MonoBehaviour
    {
        [SerializeField] private List<Bridge> _bridges;

        protected abstract void Initialize();

        public List<Bridge> GetBridges()
        {
            return _bridges;
        }
    }
}

