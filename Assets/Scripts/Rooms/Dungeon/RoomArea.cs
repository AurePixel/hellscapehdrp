﻿using UnityEngine;

namespace Hellscape.Rooms.Dungeon
{
    public class RoomArea : MonoBehaviour
    {
        protected bool _isActivated;

        public virtual void Init()
        {
            
        }
        public virtual void DesactivateArea()
        {
            _isActivated = false;
        }
        public virtual void ActivateArea()
        {
            _isActivated = true;
        }
        
        public bool isActivated()
        {
            return _isActivated;
        }
    }
}