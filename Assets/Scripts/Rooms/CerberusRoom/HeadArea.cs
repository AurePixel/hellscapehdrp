﻿using System;
using Hellscape.AttackArea;
using Hellscape.Rooms.Dungeon;
using UnityEngine;

namespace Hellscape.Rooms.CerberusRoom
{
    public class HeadArea : RoomArea
    {
        private GameObject _player;
        [SerializeField] private HeadLava _lavaArea;
        [SerializeField] private  Material _eyes;
        private GameObject _laveObj;

        private void Start()
        {
            Init();
        }

        public override void Init()
        {
            base.Init();
            _lavaArea.enabled = false;
            _eyes.SetFloat("Vector1_10F14F67",0f);
        }

        public override void ActivateArea()
        {
            base.ActivateArea();
            //_eyes.
            _lavaArea.enabled = true;
            _lavaArea.RunLava();
            _eyes.SetFloat("Vector1_10F14F67",0.9f);
        }

        public override void DesactivateArea()
        {
            base.DesactivateArea();
            _eyes.SetFloat("Vector1_10F14F67",0f);
            _lavaArea.DestroyLava();
        }

        public bool HasPlayer()
        {
            return _player;
        }

        private void OnCollisionEnter(Collision other)
        {
            _player = other.gameObject;
        }

        private void OnCollisionExit(Collision other)
        {
            _player = null;
        }
    }
}