﻿using UnityEngine;

namespace Hellscape.AttackArea
{
    public class HeadLava : Lava
    {
        private enum LavaState
        {
            Nothing=0,
            Prepare=1,
            Flow = 2,
            Disappear = 3
        }
        [SerializeField] private Animator _animator;
        [SerializeField] private GameObject _lavaObject;
        private LavaState _lavaSate;
        protected override void Init()
        {
            base.Init();
            _lavaObject.SetActive(false);
            _lavaSate = LavaState.Nothing;
        }

        public void RunLava()
        {
            _lavaObject.SetActive(true);
            ChangeLavaState(LavaState.Prepare);
            
            
        }

        public void DestroyLava()
        {
            ChangeLavaState(LavaState.Disappear);
            _audioSource.Stop();
        }

        private void ChangeLavaState(LavaState lavaState)
        {
            _lavaSate = lavaState;
            _animator.SetInteger("LavaState",(int)_lavaSate);
        }

        protected override void HandleUpdateLogic()
        {
            if (_lavaSate == LavaState.Flow)
            {
                base.HandleUpdateLogic();
                return;
            }
            if (_lavaSate == LavaState.Prepare)
            {
                float animTime = _animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
                if (_animator.GetCurrentAnimatorStateInfo(0).IsName("LavaFlow") && animTime >=0.5f)
                {
                    ChangeLavaState(_lavaSate = LavaState.Flow);
                    _audioSource.Play();
                }
                return;
            }
            if (_lavaSate == LavaState.Disappear)
            {
                float animTime = _animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
                if (_animator.GetCurrentAnimatorStateInfo(0).IsName("LavaDisappear") && animTime >=0.7f)
                {
                    ChangeLavaState(_lavaSate = LavaState.Nothing);
                    _lavaObject.SetActive(false);
                    enabled = false;
                }
            }
            
        }

        protected override void EnemyAcquired()
        {
            base.EnemyAcquired();
        }
    }
}