﻿using Hellscape.Core.Cooldown;
using UnityEngine;

namespace Hellscape.AttackArea
{
    public class Lava: AttackArea
    {
        [SerializeField] private float _lavaTickTime;
        private Cooldown _lavaTickCooldown;
        protected override void Init()
        {
            base.Init();
            _lavaTickCooldown = new Cooldown(_lavaTickTime);
        }

        protected override void HandleUpdateLogic()
        {
            if (_lavaTickCooldown.isEnded && _target)
            {
                AttackEnemy();
                _lavaTickCooldown.Start();
            }
        }

        protected override void EnemyAcquired()
        {
            
        }
    }
}