﻿using Hellscape.Core.Cooldown;
using UnityEngine;

namespace Hellscape.AttackArea
{
    public class FireColumn: Lava
    {
        [SerializeField] private float _startDelay= 2 ;
        private Cooldown _startCooldown;
        protected override void Init()
        {
            base.Init();
            _startCooldown = new Cooldown(_startDelay);
            _startCooldown.Start();
        }

        protected override void HandleUpdateLogic()
        {
            if(!_startCooldown.isEnded) return;

            base.HandleUpdateLogic();
        }

        protected override void EnemyAcquired()
        {
            base.EnemyAcquired();
        }
    }
}