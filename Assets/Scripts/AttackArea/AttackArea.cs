﻿using Hellscape.Core;
using Hellscape.Core.Cooldown;
using Hellscape.Core.Data;
using UnityEngine;

namespace Hellscape.AttackArea

{
    [RequireComponent(typeof(AudioSource))]
    public class AttackArea : MonoBehaviour
    {
        [SerializeField] private AttackData _attackData;
        [SerializeField] private AudioClip _attackSound;
        [SerializeField] private float _stayTime =-1;
        [SerializeField] private float _baseDamage =1 ;
        protected AudioSource _audioSource;
        protected Entity.Entity _target;
        [SerializeField] protected CONSTANTS.Tags _targetTag;
        private Cooldown _stayCooldown;

        private void Awake()
        {
            Init();
        }

        private void Start()
        {
            if (!_audioSource) _audioSource = GetComponent<AudioSource>();
            _stayCooldown = new Cooldown(_stayTime);
            if(_stayTime>0)_stayCooldown.Start();
        }

        protected virtual void Init() {}

        private void Update()
        {
            HandleUpdateLogic();
        }

        protected virtual void HandleUpdateLogic()
        {
            if (_stayCooldown.isEnded)
            {
                Destroy(gameObject);
            }
        }
        public void SetBaseDamage(float damage)
        {
            _baseDamage = damage;
        }

        protected virtual void EnemyAcquired()
        {
            AttackEnemy();
        }
        protected void AttackEnemy()
        {
            if (_target) _target.TakeDamage(_baseDamage * _attackData.AttackMultiplier);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!other.CompareTag(_targetTag.ToString())) return;
            _target = other.GetComponent<Entity.Entity>();
            EnemyAcquired();
        }

        private void OnTriggerExit(Collider other)
        {
            if (_target) _target = null;
        }
        
        private void OnCollisionEnter(Collision other)
        {
            if (!other.gameObject.CompareTag(_targetTag.ToString())) return;
            _target = other.gameObject.GetComponent<Entity.Entity>();
            EnemyAcquired();
        }

        private void OnCollisionExit(Collision other)
        {
            if (_target) _target = null;
        }
    }
}