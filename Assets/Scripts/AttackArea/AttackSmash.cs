﻿using Hellscape.Entity.Player;

namespace Hellscape.AttackArea
{
    public class AttackSmash : AttackArea
    {

        protected override void EnemyAcquired()
        {
            base.EnemyAcquired();
            if(_target) ((Player)_target).Jump();
            Destroy(this);
        }
    }
}