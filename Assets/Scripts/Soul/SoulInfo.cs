﻿using System;
using UnityEngine;


// life and nbre d'ames 
namespace Hellscape.Soul
{
    [Serializable]
    [CreateAssetMenu(fileName = "Soul", menuName = "SoulInfo")]
    public class SoulInfo : ScriptableObject
    {
        public SoulType _soulType;
        public String Name;
    }
}