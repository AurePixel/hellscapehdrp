﻿using System;
using Hellscape.Core;
using Hellscape.Entity.Player;
using UnityEngine;

namespace Hellscape.Soul
{
    public enum SoulType
    {
        Minion,
        Boss
    }
    public class Soul: MonoBehaviour
    {
        [SerializeField] private SoulType _soulType;
        [SerializeField] private SoulInfo _soulInfo;
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(CONSTANTS.PLAYER_TAG))
            {
                other.GetComponent<Player>().AddSouls(_soulType,_soulInfo);
                Destroy(gameObject);
            }
        }
    }
}