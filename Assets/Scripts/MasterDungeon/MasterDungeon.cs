﻿using UnityEngine;
using SDG.Platform;
using SDG.Platform.Provider;
using SDG.Platform.Entities;
using SDG.Platform.Behaviours;
using System.Collections.Generic;
using Hellscape.Rooms;
using Hellscape.Rooms.Dungeon;

namespace Hellscape.MasterDungeon
{
    public class MasterDungeon : MonoBehaviour
    {
        [SerializeField] private int _roomGenerated = 4;
        [SerializeField] private List<GameObject> _prefabRooms;
        [SerializeField] private GameObject _player;
        [SerializeField] private float _loadRange = 150;

        [SerializeField] private List<DungeonRoom> _dungeonRooms;

        private const float ROOM_SIZE_X = 34.5f;
        private const float ROOM_SIZE_Y = 22.5f;

        private void Start()
        {
            _dungeonRooms = new List<DungeonRoom>();

            RandomProvider randomProvider = new RandomProvider();
            DungeonGenerator dungeonGenerator = new DungeonGenerator(randomProvider);
            Dungeon dungeon = dungeonGenerator.Generate(_roomGenerated);

            var specialRoomList = new List<IRoomBehaviour>()
            {
                new StartRoomBehaviour(randomProvider),
                new PactRoomBehaviour(randomProvider),
                new BossRoomBehaviour(randomProvider)
            };

            dungeon = dungeonGenerator.PopulateRooms(dungeon, specialRoomList);
            GenerateDungeon(dungeon);
        }

        private void Update()
        {
            DistanceCheck();
        }

        private void GenerateDungeon(Dungeon dungeon)
        {
            /* Foreach to retireve all generated rooms in the dungeon */
            foreach (Room room in dungeon.dungeon)
            {
                if (room.RoomType != RoomType.None)
                    GenerateRoom(room);
            }
        }

        private void GenerateRoom(Room room)
        {
            if (room.RoomType == RoomType.Start || room.RoomType == RoomType.Default ||
                    room.RoomType == RoomType.Pact || room.RoomType == RoomType.Boss)
            {
                foreach (GameObject prefabRoom in _prefabRooms)
                {
                    GameObject instantiateRoom = null;

                    if (prefabRoom.tag.Equals(room.RoomType.ToString()))
                    {
                        instantiateRoom = Instantiate(prefabRoom, new Vector3(room.Pos.X * ROOM_SIZE_X, 0f, room.Pos.Y * ROOM_SIZE_Y), Quaternion.identity, transform);

                        if (room.RoomType == RoomType.Start)
                                SetPlayerPos(instantiateRoom.transform.position);

                        CheckNeighbourRooms(room, instantiateRoom.GetComponent<DungeonRoom>());
                        _dungeonRooms.Add(instantiateRoom.GetComponent<DungeonRoom>());
                    }
                }
            }
        }

        private void CheckNeighbourRooms(Room room, DungeonRoom dungeonRoom)
        {
            foreach (Bridge bridge in dungeonRoom.GetBridges())
            {
                if (!room.OpenedDoors[bridge.GetDirection()])
                {
                    Destroy(bridge.gameObject);
                    bridge.ActivateWall(true);
                }
                else
                    bridge.ActivateWall(false);
            }
        }

        private void DistanceCheck()
        {
            foreach (DungeonRoom dungeonRoom in _dungeonRooms)
            {
                if (Vector3.Distance(_player.transform.position, dungeonRoom.transform.position) < _loadRange)
                    dungeonRoom.gameObject.SetActive(true);
                else
                    dungeonRoom.gameObject.SetActive(false);
            }
        }

        private void SetPlayerPos(Vector3 pos)
        {
            _player.transform.position = pos;
        }
    }
}
