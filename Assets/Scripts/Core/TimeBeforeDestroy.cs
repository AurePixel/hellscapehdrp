﻿using System.Collections;
using UnityEngine;

namespace Hellscape.Core
{
    public class TimeBeforeDestroy : MonoBehaviour
    {
        [SerializeField] private float _time;

        private void Start()
        {
            StartCoroutine(DestroyCoroutine());
        }

        IEnumerator DestroyCoroutine()
        {
            yield return new WaitForSeconds(_time);
            Destroy(gameObject);
        }
    }
}