﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hellscape.Core
{
    public static class CONSTANTS
    {
        public const string PLAYER_TAG = "Player";
        public const string ENEMY_TAG = "Enemy";
        public const string WALL_TAG = "Wall";
        public const string GROUND_TAG = "Ground";
        public const string SOULS_COUNTER = "Souls_Counter";

        public enum Tags
        {
            Player,
            Enemy,
            Wall,
            Ground
        }
    }
}
