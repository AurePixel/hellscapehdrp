﻿using System;
using UnityEngine;

namespace Hellscape.Core.Data
{
    [CreateAssetMenu(fileName = "Data", menuName = "Sound", order = 0)]
    public class Sound : ScriptableObject
    {
        public String Name;
        public AudioClip Clip;
        [Range(0f, 1f)]
        public float Volume;
    }
}