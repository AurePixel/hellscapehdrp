﻿using UnityEngine;

namespace Hellscape.Core.Data
{
    [CreateAssetMenu(fileName = "AttackData", menuName = "Entity/Player/AttackData", order = 0)]
    public class AttackData : ScriptableObject
    {
        public Sound Sound;
        public float AttackMultiplier;
    }
}